﻿namespace CustomForms.Tests.Unit.Views.CustomForms.Fields
{
    using System;

    using ASP;

    using global::CustomForms.Models.Fields;

    using Fizzler.Systems.HtmlAgilityPack;

    using FluentAssertions;

    using NSubstitute;

    using NUnit.Framework;

    using RazorGenerator.Testing;

    [TestFixture]
    public class DateTests
    {
        #region Public Methods and Operators

        [Test]
        public void ShouldMarkFieldAsNotRequired()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void ShouldMarkFieldAsRequired()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void ShouldNotSetMaximumDateForValidation()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void ShouldNotSetMinimumDateForValidation()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void ShouldRenderDateField()
        {
            var view = new _Views_CustomForms_Fields_Date_cshtml();
            var model = Substitute.For<IDateFieldModel>();

            var html = view.RenderAsHtml(model);
            var document = html.DocumentNode;

            var fieldWrapper = document.QuerySelector(".custom-forms-field.date");
            fieldWrapper.Should().NotBeNull();

            var fieldEditorPartial = fieldWrapper.QuerySelector("partial");
            fieldEditorPartial.Should().NotBeNull();

            var dateInput = fieldWrapper.QuerySelector("input");
            dateInput.Should().NotBeNull();
            dateInput.Attributes["type"].Value.Should().Be("date");
        }

        [Test]
        public void ShouldRenderEditorOnlySettings()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void ShouldSetMaximumDateForValidation()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void ShouldSetMinimumDateForValidation()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}