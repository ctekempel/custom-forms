﻿namespace CustomForms.Extensions
{
    using System.Web.Mvc;

    public static class ExperienceEditorExtensions
    {
        #region Public Methods and Operators

        public static ExpereinceEditorFormsHelper CustomForms(this HtmlHelper htmlHelper)
        {
            return new ExpereinceEditorFormsHelper(htmlHelper);
        }

        #endregion
    }
}