﻿namespace CustomForms.Extensions
{
    using System.Web;
    using System.Web.Mvc;

    using Sitecore.Mvc;

    public class ExpereinceEditorFormsHelper
    {
        #region Fields

        private readonly HtmlHelper htmlHelper;

        #endregion

        #region Constructors and Destructors

        public ExpereinceEditorFormsHelper(HtmlHelper htmlHelper)
        {
            this.htmlHelper = htmlHelper;
        }

        #endregion

        #region Public Methods and Operators

        public HtmlString EndEditPanel()
        {
            return new HtmlString("</div>");
        }

        public HtmlString EndEditPanelSection()
        {
            return new HtmlString("</fieldset>");
        }

        public HtmlString RenderEditPanelField(string fieldName, string label)
        {
            return
                new HtmlString(string.Format("<div>{0}:{1}</div>", label, this.htmlHelper.Sitecore().Field(fieldName)));
        }

        public HtmlString StartEditPanel()
        {
            return new HtmlString("<div class=\"forms-edit-panel\">");
        }

        public HtmlString StartEditPanelSection(string label)
        {
            return new HtmlString(string.Format("<fieldset><legend>{0}</legend>", label));
        }

        #endregion
    }
}