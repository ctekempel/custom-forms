﻿namespace CustomForms.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Http;

    using Sitecore.Diagnostics;

    public class CustomFormsController : ApiController
    {
        #region Public Methods and Operators

        [Route("customForms/{formId}")]
        [HttpPost]
        public async Task<string> FindOrdersByCustomer(string formId)
        {
            Log.Info("Form:" + formId, this);

            string result = await Request.Content.ReadAsStringAsync();


            return result;
        }

        #endregion
    }
}