﻿namespace CustomForms.Constants.Templates
{
    public static class BaseField
    {
        public const string Id = "{9BE39D81-9374-4575-831C-A8896B68646C}";

        public static class Fields
        {
            public const string Required = "{D00EF352-9723-4388-AA74-AAA7BF312CB6}";
            public const string Label = "{8C815F8C-438B-43E7-9D1A-CFF81176B9DF}";
        }
    }
}