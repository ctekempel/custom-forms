﻿namespace CustomForms.Constants.Templates
{
    public static class Date
    {
        public const string Id = "{CABB98B7-088D-422A-9434-7FEB41B6C52C}";

        public static class Fields
        {
            public const string MinimumDate = "{861AD9EA-0FD7-4402-A1F4-C3A9A77AA3C8}";
            public const string MaximumDate = "{3C974D50-529C-4915-8D2F-50951B59C464}";
            
        }
    }
}