﻿namespace CustomForms.Models.Fields
{
    using System;

    using CustomForms.Constants.Templates;

    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Mvc.Presentation;

    public interface IFieldModel
    {
        bool DisplayEditingDetails { get; }

        bool Required { get; }

        Guid ItemId { get; }
    }

    public class FieldModel : RenderingModel, IFieldModel
    {
        public bool DisplayEditingDetails
        {
            get
            {
                return Sitecore.Context.PageMode.IsExperienceEditorEditing;
            }
        }

        public bool Required
        {
            get
            {
                CheckboxField value = this.Item.Fields[BaseField.Fields.Required];
                return value.Checked;
            }
        }

        public Guid ItemId
        {
            get
            {
                return this.Item.ID.Guid;
            }
        }
    }
}