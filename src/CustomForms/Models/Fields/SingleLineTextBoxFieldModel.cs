﻿namespace CustomForms.Models.Fields
{
    using CustomForms.Constants.Templates;

    public class SingleLineTextBoxFieldModel : FieldModel
    {
        public string Pattern
        {
            get
            {
                return this.Item[SingleLineTextBox.Fields.Pattern];
            }
        }
    }
}