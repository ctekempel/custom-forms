﻿namespace CustomForms.Models.Fields
{
    using System;

    using CustomForms.Constants.Templates;

    using Sitecore.Data.Fields;

    public interface IDateFieldModel : IFieldModel
    {
        DateTime? MinimumDate { get; }

        DateTime? MaximumDate { get; }
    }

    public class DateFieldModel : FieldModel, IDateFieldModel
    {
        public DateTime? MinimumDate
        {
            get
            {
                DateField value = this.Item.Fields[Date.Fields.MinimumDate];
                return value.DateTime;
            }
        }

        public DateTime? MaximumDate
        {
            get
            {
                DateField value = this.Item.Fields[Date.Fields.MaximumDate];
                return value.DateTime;
            }
        }
    }
}